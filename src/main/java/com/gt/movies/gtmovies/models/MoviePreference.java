package com.gt.movies.gtmovies.models;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * Stores preference rank of user based on RankingStrategy.
 * Also stores some de-normalized info such as movie title, cast and crew
 * to help with movie defaultSearch function
 */
@Document(value = "moviepreferences")
public class MoviePreference {

    /**
     * Key to store movie id in movie preference table
     */
    public static final String KEY_MOVIE_PREFERENCE_MOVIE_ID = "movie_id";

    /**
     * Key to store user id in movie preference table
     */
    public static final String KEY_MOVIE_PREFERENCE_USER_ID = "user_id";

    /**
     * Key to store lowercase movie's title movie preference table
     */
    public static final String KEY_MOVIE_PREFERENCE_MOVIE_TITLE = "movie_title";

    /**
     * Key to store movie's original title movie preference table
     */
    public static final String KEY_MOVIE_PREFERENCE_ORIG_MOVIE_TITLE = "original_movie_title";

    /**
     * Key to store cast
     */
    public static final String KEY_MOVIE_PREFERENCE_CAST = "cast";

    /**
     * Key to store movie's directors movie preference table
     */
    public static final String KEY_MOVIE_PREFERENCE_MOVIE_DIRECTORS = "crew";

    /**
     * Key to store movie preference's rank
     */
    public static final String KEY_MOVIE_PREFERENCE_RANK = "rank";

    @Id
    private String id;

    @Field(KEY_MOVIE_PREFERENCE_USER_ID)
    private String userId;

    @Field(KEY_MOVIE_PREFERENCE_MOVIE_ID)
    private String movieId;

    @Field(KEY_MOVIE_PREFERENCE_MOVIE_TITLE)
    private String movieTitle;

    @Field(KEY_MOVIE_PREFERENCE_ORIG_MOVIE_TITLE)
    private String originalMovieTitle;

    private List<Cast> cast;

    @Field(KEY_MOVIE_PREFERENCE_MOVIE_DIRECTORS)
    private List<Crew> crew;

    private Integer rank;


    public MoviePreference() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public String getMovieId() {
        return movieId;
    }

    public String getMovieTitle() {
        return movieTitle;
    }

    public String getOriginalMovieTitle() {
        return originalMovieTitle;
    }

    public List<Cast> getCast() {
        return cast;
    }

    public int getRank() {
        return rank;
    }

    public List<Crew> getCrew() {
        return crew;
    }

    public static class MoviePreferenceBuilder {

        private String userId;

        private String movieId;

        private String movieTitle;

        private String originalMovieTitle;

        private List<Cast> cast;

        private List<Crew> crew;

        private Integer rank;

        public MoviePreferenceBuilder(String userId, String movieId) {
            this.userId = userId;
            this.movieId = movieId;
        }

        public MoviePreferenceBuilder withMovieTitle(String title) {
            this.originalMovieTitle = title;
            this.movieTitle = title;
            return this;
        }

        public MoviePreferenceBuilder withCast(List<Cast> cast) {
            this.cast = new ArrayList<>();
            if (cast == null) {
                return this;
            }
            cast.forEach(c -> {
                Cast newCast = new Cast(c.getId(), c.getName());
                this.cast.add(newCast);
            });
            return this;
        }

        public MoviePreferenceBuilder withCrew(List<Crew> crew) {
            this.crew = new ArrayList<>();
            if (crew == null) {
                return this;
            }
            crew.forEach(c -> {
                Crew newCrew = new Crew(c.getId(), c.getName(), c.getDepartment(), c.getJob());
                this.crew.add(newCrew);
            });
            return this;
        }

        public MoviePreferenceBuilder withRank(Integer rank) {
            this.rank = rank;
            return this;
        }

        public MoviePreference build() {
            MoviePreference preference = new MoviePreference();
            preference.userId = this.userId;
            preference.movieId = this.movieId;
            preference.originalMovieTitle = this.originalMovieTitle;
            preference.movieTitle = this.movieTitle;
            preference.cast = this.cast;
            preference.crew = this.crew;
            preference.rank = this.rank;
            return preference;
        }
    }

    @Override
    public String toString() {
        return "MoviePreference{" +
                "id='" + id + '\'' +
                ", userId='" + userId + '\'' +
                ", movieId='" + movieId + '\'' +
                ", movieTitle='" + movieTitle + '\'' +
                ", rank=" + rank +
                '}';
    }
}
