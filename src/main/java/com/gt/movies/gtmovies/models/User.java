package com.gt.movies.gtmovies.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Document(value = "users")
public class User {

    /**
     * Key to store user id in user table
     */
    public static final String KEY_USER_USER_ID = "user_id";

    /**
     * Key to store preferred languages in user table
     */
    public static final String KEY_USER_PREFERRED_LANGUAGES = "preferred_languages";

    /**
     * Key to store favorite actor in user table
     */
    public static final String KEY_USER_FAVORITE_ACTORS = "favotire_actors";

    /**
     * Key to store favorite actor in user table
     */
    public static final String KEY_USER_FAVORITE_DIRECTORS = "favotire_directors";

    /**
     * Key to store top 3 preferred movies
     */
    public static final String KEY_TOP_MOVIES = "top_movies";

    @Id
    private String id;

    @Field(KEY_USER_USER_ID)
    private String userId;

    @Field(KEY_USER_PREFERRED_LANGUAGES)
    private List<String> preferredLanguages;

    @Field(KEY_USER_FAVORITE_ACTORS)
    private List<String> favActors;

    @Field(KEY_USER_FAVORITE_DIRECTORS)
    private List<String> favDirectors;

    @Field(KEY_TOP_MOVIES)
    private Set<String> topMovies = new HashSet<>();

    private User() {

    }

    public String getId() {
        return id;
    }


    public String getUserId() {
        return userId;
    }


    public List<String> getPreferredLanguages() {
        return preferredLanguages;
    }


    public List<String> getFavActors() {
        return favActors;
    }


    public List<String> getFavDirectors() {
        return favDirectors;
    }

    public Set<String> getTopMovies() {
        return topMovies;
    }

    public static class UserBuilder {

        private String userId;

        private List<String> preferredLanguages;

        private List<String> favActors;

        private List<String> favDirectors;

        private Set<String> topMovies;

        public UserBuilder(String userId) {
            this.userId = userId;
        }

        public UserBuilder withPreferredLanguages(List<String> preferredLanguages) {
            this.preferredLanguages = preferredLanguages;
            return this;
        }

        public UserBuilder withfavoriteActors(List<String> favActors) {
            this.favActors = favActors;
            return this;
        }

        public UserBuilder withfavoriteDirectors(List<String> favDirectors) {
            this.favDirectors = favDirectors;
            return this;
        }

        public UserBuilder withTopMovies(Set<String> topMovies) {
            this.topMovies = topMovies;
            return this;
        }

        public User build() {
            User user = new User();
            user.userId = this.userId;
            user.preferredLanguages = this.preferredLanguages;
            user.favActors = this.favActors;
            user.favDirectors = this.favDirectors;
            user.topMovies = this.topMovies;
            return user;
        }


    }

    @Override
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                ", userId='" + userId + '\'' +
                ", topMovies=" + topMovies +
                '}';
    }
}
