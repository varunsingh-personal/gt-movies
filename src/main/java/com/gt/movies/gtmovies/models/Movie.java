package com.gt.movies.gtmovies.models;

import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Document to store movie
 */
@Document(value = "movies")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Movie {

    /**
     * Key to store user id in user table
     */
    public static final String KEY_MOVIE_MOVIE_ID = "movie_id";

    @Id
    private String id;

    @Field(KEY_MOVIE_MOVIE_ID)
    private String movieId;

    private String title;

    private List<Cast> cast;

    private List<Crew> crew;

    private String language;

    /**
     * private constructor to avoid direct object creation
     */
    private Movie() {
    }

    public String getId() {
        return id;
    }

    public String getMovieId() {
        return movieId;
    }

    public String getTitle() {
        return title;
    }

    public List<Cast> getCast() {
        return cast;
    }

    public List<Crew> getCrew() {
        return crew;
    }

    public String getLanguage() {
        return language;
    }

    /**
     * Constructs movie object
     */
    public static class MovieBuilder {

        private ObjectMapper objectMapper;

        private String movieId;

        private String title;

        private List<Cast> cast;

        private List<Crew> crew;

        private String language;

        private Logger logger = LoggerFactory.getLogger(this.getClass());


        public MovieBuilder(String id) {
            this.movieId = id;
            this.objectMapper = new ObjectMapper();
        }



        public MovieBuilder withTitle(String title) {
            this.title = title;
            return this;
        }

        public MovieBuilder withLanguage(String language) {
            this.language = language;
            return this;
        }

        /**
         * Takes json string and converts into list of Cast objects
         * @param cast
         * @throws IOException
         */
        public MovieBuilder withCast(String cast) {
            if (cast == null) {
                return this;
            }
            try {
                this.cast = this.objectMapper.readValue(cast, new TypeReference<List<Cast>>(){});
            } catch (IOException e) {
            	this.logger.error(e.getMessage());
            }
            return this;
        }

        /**
         * Takes json string and converts into list of Crew objects
         * @param crew
         * @throws IOException
         */
        public MovieBuilder withCrew(String crew) {
            if (crew == null) {
                return this;
            }
            try {
                this.crew = this.objectMapper.readValue(crew, new TypeReference<List<Crew>>(){});
            } catch (IOException e) {
                this.logger.error(e.getMessage());
            }
            return this;

        }

        public Movie build() {
            Movie movie = new Movie();
            movie.movieId = this.movieId;
            movie.title = this.title;
            movie.cast = this.cast;
            movie.crew = this.crew;
            movie.language = language;
            return movie;
        }
    }

    @Override
    public String toString() {
        return "Movie{" +
                "id='" + id + '\'' +
                ", title='" + title + '\'' +
                '}';
    }
}
