package com.gt.movies.gtmovies.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Crew {

    private String id;

    private String name;

    private String department;

    private String job;

    public Crew() {
    }

    public Crew(String name) {
        this.name = name;
    }

    public Crew(String name, String department, String job) {
        this.name = name;
        this.department = department;
        this.job = job;
    }

    public Crew( String id, String name, String department, String job) {
        this.id = id;
        this.name = name;
        this.department = department;
        this.job = job;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    @Override
    public String toString() {
        return "Crew{" +
                "name='" + name + '\'' +
                ", job='" + job + '\'' +
                '}';
    }
}
