package com.gt.movies.gtmovies.repositories;

import com.gt.movies.gtmovies.models.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends MongoRepository<User, String> {

    public User findByUserId(String userId);

}
