package com.gt.movies.gtmovies.repositories;

import java.util.List;
import java.util.stream.Stream;

import com.gt.movies.gtmovies.models.Movie;
import com.gt.movies.gtmovies.models.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.TextCriteria;
import org.springframework.data.mongodb.core.query.TextQuery;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StopWatch;

import com.gt.movies.gtmovies.models.MoviePreference;

@Component
public class MoviesRegexSearchRepositoryImpl implements MoviesRegexSearchRepository {

    private Logger logger = LoggerFactory.getLogger(this.getClass().getSimpleName());
    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private Environment environment;


    @Override
    public Stream<MoviePreference> defaultSearch(String userId, List<String> searchTerms, int minRank, int maxRank) {
        String fuzzySearchText = this.joinSearchTerms(" ", searchTerms);
        String regexSearchText = this.joinSearchTerms("|", searchTerms);
        TextCriteria fullTextCriteria = TextCriteria.forDefaultLanguage().caseSensitive(false).matching(fuzzySearchText);
        Criteria criteria = new Criteria()
                .andOperator(
                        Criteria.where(MoviePreference.KEY_MOVIE_PREFERENCE_USER_ID).is(userId),
                        new Criteria().orOperator(
                                Criteria.where(MoviePreference.KEY_MOVIE_PREFERENCE_CAST).elemMatch(Criteria.where("name").regex(regexSearchText, "i")),
                                Criteria.where(MoviePreference.KEY_MOVIE_PREFERENCE_MOVIE_TITLE).regex(regexSearchText, "i"),
                                Criteria.where(MoviePreference.KEY_MOVIE_PREFERENCE_MOVIE_DIRECTORS).elemMatch(
                                        Criteria.where("name").regex(regexSearchText, "i")
                                                .and("department").is("Directing")
                                                .and("job").is("Director"))
                        ),
                        Criteria.where(MoviePreference.KEY_MOVIE_PREFERENCE_RANK).gte(minRank).lte(maxRank)
                );
        Query query = TextQuery.queryText(fullTextCriteria);
        query.addCriteria(criteria);
        List<MoviePreference> results = this.mongoTemplate.find(query, MoviePreference.class);
        return results.stream();
    }

    private String joinSearchTerms(String joinString, List<String> searchTerms) {
        if (!CollectionUtils.isEmpty(searchTerms)) {
            return String.join(joinString, searchTerms);
        }

        return "";
    }
}
