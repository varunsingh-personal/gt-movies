package com.gt.movies.gtmovies.repositories;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.gt.movies.gtmovies.models.MoviePreference;

@Repository
public interface MoviePreferenceRepository extends MongoRepository<MoviePreference, String>, MoviesRegexSearchRepository {

    public List<MoviePreference> findByUserIdOrderByRankDesc(String userId);

}
