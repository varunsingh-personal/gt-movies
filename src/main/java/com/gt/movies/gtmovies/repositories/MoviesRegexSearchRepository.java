package com.gt.movies.gtmovies.repositories;

import com.gt.movies.gtmovies.models.MoviePreference;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Stream;

@Repository
public interface MoviesRegexSearchRepository {

    /**
     * Searches for movies in MoviePreference collection
     * @param userId user\"s id. Movie preference will be filtered by this id
     * @param searchTerms defaultSearch terms. Terms will searched considering OR between them
     * @param minRank minimum preference rank a movie preference should have to make it into the results
     * @param maxRank maximum preference rank a movie preference should have to make it into the results
     * @return Stream of movie preference
     */
    public Stream<MoviePreference> defaultSearch(String userId, List<String> searchTerms, int minRank, int maxRank);
}
