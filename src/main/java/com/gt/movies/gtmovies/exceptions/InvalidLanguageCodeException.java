package com.gt.movies.gtmovies.exceptions;

public class InvalidLanguageCodeException extends ApiException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3741012893190943509L;

	public InvalidLanguageCodeException(String message, String errorCode) {
        super(message, errorCode);
    }
}
