package com.gt.movies.gtmovies.exceptions;

public class ObjectDoesNotExistException extends ApiException {

    /**
	 * 
	 */
	private static final long serialVersionUID = 4317054418778323460L;

	public ObjectDoesNotExistException(String message, String errorCode) {
        super(message, errorCode);
    }
}
