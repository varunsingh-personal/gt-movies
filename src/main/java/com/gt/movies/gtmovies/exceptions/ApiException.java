package com.gt.movies.gtmovies.exceptions;

public class ApiException extends Exception {

    /**
	 * 
	 */
	private static final long serialVersionUID = -6858333880467120896L;
	private String errorCode;

    public ApiException(String message, String errorCode) {
        super(message);
        this.errorCode = errorCode;
    }

    public String getErrorCode() {
        return errorCode;
    }
}
