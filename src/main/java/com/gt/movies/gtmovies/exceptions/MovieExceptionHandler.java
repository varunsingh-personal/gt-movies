package com.gt.movies.gtmovies.exceptions;

import com.gt.movies.gtmovies.dtos.ApiError;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class MovieExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = SearchValidationException.class)
    public ResponseEntity<ApiError> handleValidationException(SearchValidationException ex, WebRequest webRequest) {
    	return new ResponseEntity<>(ApiError.of(ex.getErrorCode(), ex.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = ObjectDoesNotExistException.class)
    public ResponseEntity<ApiError> handleObjectDoesNotExist(ObjectDoesNotExistException ex, WebRequest webRequest) {
    	return new ResponseEntity<>(ApiError.of(ex.getErrorCode(), ex.getMessage()), HttpStatus.NOT_FOUND);
    }
}
