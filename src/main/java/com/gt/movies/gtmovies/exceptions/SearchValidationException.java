package com.gt.movies.gtmovies.exceptions;

public class SearchValidationException extends ApiException {


    /**
	 * 
	 */
	private static final long serialVersionUID = 1226645901193062245L;

	public SearchValidationException(String message, String errorCode) {
        super(message, errorCode);
    }
}
