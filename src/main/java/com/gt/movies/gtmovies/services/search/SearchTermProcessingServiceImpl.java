package com.gt.movies.gtmovies.services.search;

import com.gt.movies.gtmovies.exceptions.SearchValidationException;
import com.gt.movies.gtmovies.utils.ErrorCodes;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class SearchTermProcessingServiceImpl implements SearchTermProcessingService {

    @Override
    public List<String> processSearchTerms(String searchText) throws SearchValidationException {
        if (StringUtils.isEmpty(searchText)) {
            throw new SearchValidationException("invalid search term", ErrorCodes.ERROR_CODE_INVALID_SEARCH_TERMS);
        }
//        Set<String> searchTerms = new HashSet<>(Arrays.asList(searchText.split(",")));
        List<String> searchTerms = Arrays.asList(searchText.split(","));
        return searchTerms.stream().map(String::trim).map(String::toLowerCase).collect(Collectors.toList());
    }
}
