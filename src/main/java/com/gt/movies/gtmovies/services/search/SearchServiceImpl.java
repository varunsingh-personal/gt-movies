package com.gt.movies.gtmovies.services.search;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import com.gt.movies.gtmovies.exceptions.ObjectDoesNotExistException;
import com.gt.movies.gtmovies.exceptions.SearchValidationException;
import com.gt.movies.gtmovies.models.MoviePreference;
import com.gt.movies.gtmovies.repositories.MoviePreferenceRepository;
import org.springframework.util.StopWatch;

@Component
public class SearchServiceImpl implements SearchService {

    private final SearchTermProcessingService searchTermProcessingService;
    private MoviePrefComparator moviePrefComparator;
    private MoviePreferenceRepository moviePreferenceRepository;
    private SearchValidationService searchValidationService;
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    public SearchServiceImpl(MoviePreferenceRepository moviePreferenceRepository,
                             SearchValidationService searchValidationService,
                             SearchTermProcessingService searchTermProcessingService,
                             MoviePrefComparator moviePrefComparator) {
        this.moviePreferenceRepository = moviePreferenceRepository;
        this.searchValidationService = searchValidationService;
        this.searchTermProcessingService = searchTermProcessingService;
        this.moviePrefComparator = moviePrefComparator;

    }
    @Override
    @Cacheable("movies")
    public List<String> searchMovies(String userId, String searchText) throws ObjectDoesNotExistException, SearchValidationException {
        this.logger.info("Searching for movies...");
        List<String> searchTerms = this.searchTermProcessingService.processSearchTerms(searchText);
        this.logger.info("Processed search terms: {}", searchTerms);
        this.searchValidationService.validateSearchRequest(userId, searchTerms);
        this.logger.info("Search validation passed");

        StopWatch sw = new StopWatch();
        sw.start();
        List<MoviePreference> results = this.moviePreferenceRepository.defaultSearch(userId, searchTerms, 0,Integer.MAX_VALUE).collect(Collectors.toList());
        sw.stop();
        this.logger.debug("time take to search: {}", sw.getLastTaskTimeMillis());

        sw.start();
        List<MoviePreference> preferredMovies = results.stream().filter(mf -> mf.getRank() > 0).sorted(moviePrefComparator).collect(Collectors.toList());
        sw.stop();
        this.logger.debug("time take to sort preferred: {}", sw.getLastTaskTimeMillis());

        sw.start();
        List<MoviePreference> movies = results.stream().filter(mf -> mf.getRank() == 0).sorted(moviePrefComparator).collect(Collectors.toList());
        sw.stop();
        this.logger.debug("time taken to sort movies: {}", sw.getLastTaskTimeMillis());

        this.logger.trace("Preferred movies: {}", preferredMovies);
        this.logger.trace("Movies: {}", movies);
        List<String> retVal = new ArrayList<>(preferredMovies.size() + movies.size());
        retVal.addAll(preferredMovies.stream().map(MoviePreference::getOriginalMovieTitle).collect(Collectors.toList()));
        retVal.addAll(movies.stream().map(MoviePreference::getOriginalMovieTitle).collect(Collectors.toList()));

        return retVal;
    }
}
