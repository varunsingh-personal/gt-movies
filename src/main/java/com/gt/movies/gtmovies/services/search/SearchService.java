package com.gt.movies.gtmovies.services.search;

import com.gt.movies.gtmovies.exceptions.ObjectDoesNotExistException;
import com.gt.movies.gtmovies.exceptions.SearchValidationException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface SearchService {

    /**
     * Searches movies based on search terms
     * @param userId
     * @param searchTerms
     * @return
     */
    public List<String> searchMovies(String userId, String searchTerms) throws SearchValidationException, ObjectDoesNotExistException;
}
