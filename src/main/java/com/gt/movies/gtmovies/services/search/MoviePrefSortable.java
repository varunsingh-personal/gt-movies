package com.gt.movies.gtmovies.services.search;

import com.gt.movies.gtmovies.models.MoviePreference;

import java.util.List;

public interface MoviePrefSortable {

    /**
     * Sorts search results
     * @param searchResults results to sort
     * @return sorted results
     */
    public List<String> sortSearchResults(List<MoviePreference> searchResults);
}
