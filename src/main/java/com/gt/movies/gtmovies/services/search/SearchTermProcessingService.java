package com.gt.movies.gtmovies.services.search;

import com.gt.movies.gtmovies.exceptions.SearchValidationException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface SearchTermProcessingService {

    /**
     * Processes search terms received from request so they can searched in the database
     * @param searchTerms
     * @return List of processed search terms
     */
    List<String> processSearchTerms(String searchTerms) throws SearchValidationException;
}
