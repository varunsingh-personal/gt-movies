package com.gt.movies.gtmovies.services.search;

import com.gt.movies.gtmovies.models.MoviePreference;
import org.springframework.stereotype.Component;

@Component
public class MovieTitleComparatorImpl implements MoviePrefComparator {

    @Override
    public int compare(MoviePreference mpLeft, MoviePreference mpRight) {
        return mpLeft.getMovieTitle().compareTo(mpRight.getMovieTitle());
    }
}
