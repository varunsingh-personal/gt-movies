package com.gt.movies.gtmovies.services.user;

import com.gt.movies.gtmovies.dtos.MoviePrefDto;
import com.gt.movies.gtmovies.models.User;
import com.gt.movies.gtmovies.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

@Component
public class UserServiceImpl implements UserService {

    private UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<MoviePrefDto> getTopMovies() {
        Stream<User> users = this.userRepository.findAll().stream();
        final List<MoviePrefDto> topMovies = new ArrayList<>();
        users.forEach(user -> {
            topMovies.add(MoviePrefDto.of(user));
        });
        return topMovies;
    }
}
