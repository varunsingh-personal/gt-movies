package com.gt.movies.gtmovies.services.search;

import com.gt.movies.gtmovies.exceptions.ObjectDoesNotExistException;
import com.gt.movies.gtmovies.exceptions.SearchValidationException;
import com.gt.movies.gtmovies.models.User;
import com.gt.movies.gtmovies.repositories.UserRepository;
import com.gt.movies.gtmovies.utils.ErrorCodes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.List;

@Component
public class SearchValidationServiceImpl implements SearchValidationService {

    /**
     * User repo
     */
    private UserRepository userRepository;

    @Autowired
    public SearchValidationServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public void validateSearchRequest(String userId, List<String> searchTerms) throws ObjectDoesNotExistException, SearchValidationException {
        this.validateUser(userId);
        this.validateSearchTerms(searchTerms);
    }


    /**
     * Checks if user id is valid
     * @param userId
     * @throws SearchValidationException
     */
    private void validateUser(String userId) throws ObjectDoesNotExistException {
        User user = this.userRepository.findByUserId(userId);
        if (user == null) {
            throw new ObjectDoesNotExistException("user not found", ErrorCodes.ERROR_CODE_USER_NOT_FOUND);
        }
    }

    /**
     * Validates search terms
     * 1. Checks if searchTerms is null
     * 2. Checks if any term in searchTerms is null
     * @param searchTerms
     * @throws SearchValidationException
     */
    private void validateSearchTerms(List<String> searchTerms) throws SearchValidationException {
        if (searchTerms == null) {
            throw new SearchValidationException("invalid search terms", ErrorCodes.ERROR_CODE_INVALID_SEARCH_TERMS);
        }

        for (String term: searchTerms) {
            if (StringUtils.isEmpty(term)) {
                throw new SearchValidationException("invalid search terms", ErrorCodes.ERROR_CODE_INVALID_SEARCH_TERMS);
            }
        }
    }
}
