package com.gt.movies.gtmovies.services.user;

import com.gt.movies.gtmovies.dtos.MoviePrefDto;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface UserService {

    public List<MoviePrefDto> getTopMovies();
}
