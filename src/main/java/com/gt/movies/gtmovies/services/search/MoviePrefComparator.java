package com.gt.movies.gtmovies.services.search;

import com.gt.movies.gtmovies.models.MoviePreference;
import org.springframework.stereotype.Component;

import java.util.Comparator;

@Component
public interface MoviePrefComparator extends Comparator<MoviePreference> {
}
