package com.gt.movies.gtmovies.services.search;

import com.gt.movies.gtmovies.exceptions.ObjectDoesNotExistException;
import com.gt.movies.gtmovies.exceptions.SearchValidationException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface SearchValidationService {

    /**
     * Validates search inputs. Throws exception in validation fails
     * @param userId id of user searching for movies
     * @param searchTerms searchTerms passed
     * @throws SearchValidationException if validation fails
     */
    public void validateSearchRequest(String userId, List<String> searchTerms) throws SearchValidationException, ObjectDoesNotExistException;
}
