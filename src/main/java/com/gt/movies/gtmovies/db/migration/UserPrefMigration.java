package com.gt.movies.gtmovies.db.migration;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.index.Index;
import org.springframework.data.mongodb.core.index.TextIndexDefinition;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.data.util.StreamUtils;

import com.github.mongobee.changeset.ChangeLog;
import com.github.mongobee.changeset.ChangeSet;
import com.gt.movies.gtmovies.models.Movie;
import com.gt.movies.gtmovies.models.MoviePreference;
import com.gt.movies.gtmovies.models.User;
import com.gt.movies.gtmovies.rank_processing.DefaultRankingStrategy;
import com.gt.movies.gtmovies.rank_processing.MovieRankingService;
import com.gt.movies.gtmovies.rank_processing.MovieRankingServiceImpl;
import com.gt.movies.gtmovies.rank_processing.RankingStrategy;
import com.gt.movies.gtmovies.utils.Constants;

@ChangeLog(order = "201904241721")
public class UserPrefMigration {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * Calculates the preference of all movies for all users and stores in a document
     * Logic to calculate preference rank of a movie for a user:
     * 1. Search all pref langs in movie. Add count of matches * weightage as score
     * 2. Search all pref actors in movie. Add count of matches * weightage as score
     * 3. Search all pref directors in movie. Add count of matches * weightage as score
     * 4. Add all scores and assign
     * @param mongoTemplate
     * @param environment
     */
    @ChangeSet(order = "201904241721", id = "calculateMovieRank", author = "developer")
    @Profile({"dev", "test", "prod"})
    public void calculatePrefRank(MongoTemplate mongoTemplate, Environment environment) {

        this.logger.info("Starting movie preference calculation");

        Query allUsersQuery = new Query();
        allUsersQuery.addCriteria(Criteria.where("user_id").regex(".*"));
        Stream<User> allUserStream = StreamUtils.createStreamFromIterator(mongoTemplate.stream(allUsersQuery, User.class));
        this.logger.info("Going to loop through all users and movies to calculate rank");
        allUserStream.forEach(user -> {
            this.logger.debug("current user: {}", user);
            final User thisUser = user;
            Query allMoviesQuery = new Query();
            allMoviesQuery.addCriteria(Criteria.where("movie_id").regex(".*"));
            Stream<Movie> allMovieStream = StreamUtils.createStreamFromIterator(mongoTemplate.stream(allMoviesQuery, Movie.class));
            allMovieStream.forEach(movie -> {
                this.logger.debug("current movie: {}", movie);
                RankingStrategy rankingStrategy = new DefaultRankingStrategy(movie, thisUser);
                MovieRankingService rankingService = new MovieRankingServiceImpl();
                int rank = rankingService.rankMovie(rankingStrategy);
                this.logger.debug("calculated rank: {}", rank);
                MoviePreference.MoviePreferenceBuilder builder = new MoviePreference.MoviePreferenceBuilder(thisUser.getUserId(), movie.getMovieId());
                MoviePreference preference = builder
                        .withMovieTitle(movie.getTitle())
                        .withCast(movie.getCast())
                        .withCrew(movie.getCrew())
                        .withRank(rank)
                        .build();
                mongoTemplate.save(preference);

            });
        });
    }


    @ChangeSet(order = "201907202319", id = "addTopMovies", author = "developer")
    @Profile({"dev", "test", "prod"})
    public void addTopMoviesToUser(MongoTemplate mongoTemplate, Environment environment) {

        this.logger.info("Calculating top movies");

        final Map<String, List<MoviePreference>> userMap = new HashMap<>();
        mongoTemplate.findAll(MoviePreference.class).forEach(moviePreference -> {
            String userId = moviePreference.getUserId();
            int rank = moviePreference.getRank();
            if (rank > 0) {
                if (!userMap.containsKey(userId)) {
                    List<MoviePreference> topMoviePrefs = new ArrayList<>();
                    topMoviePrefs.add(moviePreference);
                    userMap.put(userId, topMoviePrefs);
                } else {
                    userMap.get(userId).add(moviePreference);
                }
            }
        });
        this.logger.info("Calculated top movies. Adding top movies to users db");
        userMap.forEach((userId, movies) -> {
        	logger.debug("movies: {}", movies);
        	Comparator<MoviePreference> cmp = (MoviePreference o1, MoviePreference o2) -> {
					return o2.getRank() - o1.getRank();
			};
			movies.sort(cmp);
			logger.debug("after sorting movies: {}", movies);
			List<String> topMovies = new ArrayList<>();
			for (int i = 0; i < Constants.MAX_TOP_MOVIES && movies.size() > i; i++) {
				topMovies.add(movies.get(i).getOriginalMovieTitle());
			}
			this.logger.debug("top Movies: {}", topMovies);
			
            Query query = new Query();
            Criteria criteria = Criteria.where(User.KEY_USER_USER_ID).is(userId);
            query.addCriteria(criteria);
            Update update = new Update();
            update.set(User.KEY_TOP_MOVIES, topMovies);
            mongoTemplate.findAndModify(query, update, User.class);
        });


    }

    @ChangeSet(order = "201909010935", id = "addIndexesToMoviePreferences", author = "developer")
    @Profile({"dev", "test", "prod"})
    public void createMoviePrefIndices(MongoTemplate mongoTemplate, Environment environment) {

        this.logger.info("Adding fulltext indexes");

        TextIndexDefinition textIndexDefinition = new TextIndexDefinition.TextIndexDefinitionBuilder()
                .onFields("cast.name", "crew.name", "movie_title")
                .withDefaultLanguage("en")
                .build();
        mongoTemplate.indexOps(MoviePreference.class).ensureIndex(textIndexDefinition);

        this.logger.info("Fulltext index added on cast.name, crew.name and movie_title");

        Index rankIndex = new Index();
        rankIndex.on(MoviePreference.KEY_MOVIE_PREFERENCE_RANK, Sort.Direction.DESC);
        mongoTemplate.indexOps(MoviePreference.class).ensureIndex(rankIndex);

    }
}
