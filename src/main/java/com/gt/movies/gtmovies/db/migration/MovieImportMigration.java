package com.gt.movies.gtmovies.db.migration;

import com.github.mongobee.changeset.ChangeLog;
import com.github.mongobee.changeset.ChangeSet;
import com.gt.movies.gtmovies.models.Movie;
import com.opencsv.CSVReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

@ChangeLog(order = "201904210955")
public class MovieImportMigration {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * Imports movies into mongodb from combined csv of inputs files
     * @param mongoTemplate
     * @param environment
     */
    @ChangeSet(order = "201904210955", id = "importMoviesCsv", author = "developer")
    @Profile({"dev", "test", "prod"})
    public void importMovies(MongoTemplate mongoTemplate, Environment environment) {
        this.logger.info("importMoviesCsv migration started");
        final String filePath = environment.getProperty("com.gt.movies.gtmovies.seedfilepath");
        if (filePath == null) {
            this.logger.error("file path null. Could not find csv .file to import from. Returning.");
            return;
        }
        this.logger.info("file path: {}", filePath);
        try (CSVReader csvReader = new CSVReader(new FileReader(filePath))) {
            String[] currRow = csvReader.readNext(); //Ignore header row
            if (currRow != null) {
                currRow = csvReader.readNext();
            }
            while (currRow != null) {
                String id = String.valueOf(currRow[3]);
                String lang = String.valueOf(currRow[5]);
                String title = String.valueOf(currRow[20]);
                String cast = String.valueOf(currRow[21]);
                String crew = String.valueOf(currRow[22]);
                Movie.MovieBuilder builder = new Movie.MovieBuilder(id)
                        .withTitle(title)
                        .withLanguage(lang)
                        .withCast(cast)
                        .withCrew(crew);
                Movie movie = builder.build();
                Movie savedMovie = mongoTemplate.save(movie);
                this.logger.debug("Successfully saved Movie: {}", savedMovie);
                currRow = csvReader.readNext();
            }

        } catch (FileNotFoundException e) {
            logger.error("File not found");
            this.logger.error(e.getLocalizedMessage());
        } catch (IOException e) {
            logger.error("Something went wrong with IO");
            this.logger.error(e.getLocalizedMessage());
        }
        this.logger.info("importMoviesCsv migration ended");

    }
}
