package com.gt.movies.gtmovies.db.migration;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.github.mongobee.changeset.ChangeLog;
import com.github.mongobee.changeset.ChangeSet;
import com.gt.movies.gtmovies.models.User;

/**
 * Imports users from seed json .file
 */
@ChangeLog(order = "201904231725")
public class UserImportMigration {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    /**
     * Imports users into mongodb from user_preferences.json
     * @param mongoTemplate
     * @param environment
     */
    @ChangeSet(order = "201904231725", id = "importUsers", author = "developer")
    @Profile({"dev", "test", "prod"})
    public void importUsers(MongoTemplate mongoTemplate, Environment environment) {
        this.logger.info("importusers migration started");
        ObjectMapper objectMapper = new ObjectMapper();
        String filePath = environment.getProperty("com.gt.movies.gtmovies.userseedfilepath");
        this.logger.info("Seed .file path: {}", filePath);
        if (filePath == null) {
            this.logger.error(".file path null. Could not find user .file to load data from. Returning. ");
            return;
        }
        this.logger.info("file path: {}", filePath);
        try {
            ArrayNode nodes = (ArrayNode) objectMapper.readTree(new File(filePath));
            this.logger.debug("printing nodes");
            nodes.forEach(node -> {
                this.logger.debug(node.asText());
                Iterator<Map.Entry<String, JsonNode>> iterator = node.fields();
                while (iterator.hasNext()) {
                    Map.Entry<String, JsonNode> entry = iterator.next();
                    String id = entry.getKey();
                    List<String> preferredLangs = this.parsePreferences("preferred_languages", entry.getValue());
                    List<String> favActors = this.parsePreferences("favourite_actors", entry.getValue());
                    List<String> favDirectors = this.parsePreferences("favourite_directors", entry.getValue());

                    User.UserBuilder builder = new User.UserBuilder(id);
                    User user = builder.withPreferredLanguages(preferredLangs)
                            .withfavoriteActors(favActors)
                            .withfavoriteDirectors(favDirectors).build();
                    User savedUser = mongoTemplate.save(user);
                    this.logger.debug("Saved user: {}", savedUser);
                }
            });
        } catch (FileNotFoundException e) {
            this.logger.error("File not found. Could not load data");
            this.logger.error(e.getLocalizedMessage());
        } catch (IOException e) {
            this.logger.error("Something went wrong with IO. Could not load data");
            this.logger.error(e.getLocalizedMessage());
        }

        this.logger.info("importusers migration completed");

    }

    /**
     * parses user preferences for import users from seed .file
     * @param key key of array node from which preferences are to be extracted
     * @param node array node from which preferences are to be extracted
     * @return list of preferences
     */
    private List<String> parsePreferences(String key, JsonNode node) {
        List<String> prefList = new ArrayList<>();
        ArrayNode prefNodes = (ArrayNode) node.get(key);
        for (JsonNode n: prefNodes) {
            prefList.add(n.asText());
        }
        return prefList;
    }

}
