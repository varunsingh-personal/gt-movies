package com.gt.movies.gtmovies.rank_processing;

import org.springframework.stereotype.Service;

/**
 * performs rank calculation of a movie for a user
 */
@Service
public interface MovieRankingService {

    /**
     * return a rank integer based on RankingStrategy
     * @param rankingStrategy ranking strategy
     * @return rank of the movie
     */
    public int rankMovie(RankingStrategy rankingStrategy);
}
