package com.gt.movies.gtmovies.rank_processing;

import com.gt.movies.gtmovies.exceptions.InvalidLanguageCodeException;
import com.gt.movies.gtmovies.models.Cast;
import com.gt.movies.gtmovies.models.Crew;
import com.gt.movies.gtmovies.models.Movie;
import com.gt.movies.gtmovies.models.User;
import com.gt.movies.gtmovies.utils.LanguageUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Stream;

/**
 * Default ranking strategy. Considers users preference in language, actor and director
 * Ranks by
 * language * language weightage + num of actors match * weightage of actor + num of directors match * weightage of director
 */
@Component
public class DefaultRankingStrategy implements RankingStrategy {

    /**
     * Weightage to be given to language to calculate rank
     */
    public static final int LANGUAGE_WEIGHTAGE = 0;

    /**
     * Weightage to be given to actor to calculate rank
     */
    public static final int ACTOR_WEIGHTAGE = 1;

    /**
     * Weightage to be given to director to calculate rank
     */
    public static final int DIRECTOR_WEIGHTAGE = 1;

    private Movie movie;

    private User user;

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private DefaultRankingStrategy() {
    }

    public DefaultRankingStrategy(Movie movie, User user) {
        this.movie = movie;
        this.user = user;
    }

    /**
     *
     * @return lang * LANGUAGE_WEIGHTAGE + sum_actor_matches * ACTOR_WEIGHTAGE + sum_director_matches * DIRECTOR_WEIGHTAGE
     */
    @Override
    public int rank() {
        logger.debug("performing rank calc for: {} for movie: {}", user, movie);
        int retVal = 0;
        List<Cast> casts = this.movie.getCast();
        List<Crew> crews = this.movie.getCrew();
        String langCode = this.movie.getLanguage();
        String language = null;
        if (langCode != null) {
            try {
                language = LanguageUtils.languageFromCode(langCode);
            } catch (InvalidLanguageCodeException e) {
                this.logger.warn(e.getLocalizedMessage());
            }
        }


        final List<String> favActors = this.user.getFavActors();
        final List<String> favDirectors = this.user.getFavDirectors();
        final List<String> prefLangs = this.user.getPreferredLanguages();

        final String finalLang = language;

        if (finalLang != null && prefLangs!= null && prefLangs.stream().anyMatch(lang -> lang.equalsIgnoreCase(finalLang))) {
            retVal += DefaultRankingStrategy.LANGUAGE_WEIGHTAGE;
        }

        if (favActors != null && casts != null) {
            Stream<Cast> favCast = casts.stream().filter(cast -> favActors.contains(cast.getName()));
            retVal += DefaultRankingStrategy.ACTOR_WEIGHTAGE * favCast.count();

        }


        if (favDirectors != null && crews != null) {
            Stream<Crew> favCrew = crews.stream().filter(crew -> favDirectors.contains(crew.getName())
                    && crew.getJob().equalsIgnoreCase("Director"));
            retVal += DefaultRankingStrategy.DIRECTOR_WEIGHTAGE * favCrew.count();
        }

        return retVal;
    }
}
