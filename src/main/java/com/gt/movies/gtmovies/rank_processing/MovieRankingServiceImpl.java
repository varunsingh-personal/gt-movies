package com.gt.movies.gtmovies.rank_processing;

import org.springframework.stereotype.Component;

@Component
public class MovieRankingServiceImpl implements MovieRankingService {

    public MovieRankingServiceImpl() {
    }

    @Override
    public int rankMovie(RankingStrategy rankingStrategy) {
        return rankingStrategy.rank();
    }
}
