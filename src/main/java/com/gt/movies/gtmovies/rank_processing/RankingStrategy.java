package com.gt.movies.gtmovies.rank_processing;

import org.springframework.stereotype.Service;

/**
 * Interface defines a ranking strategy used to rank
 * movies
 */
@Service
public interface RankingStrategy {

    /**
     * Returns rank of an item
     * @return rank
     */
    public int rank();
}
