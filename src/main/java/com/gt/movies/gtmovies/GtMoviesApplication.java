package com.gt.movies.gtmovies;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;


@SpringBootApplication
@ComponentScan("com.gt.movies.gtmovies")
@EnableCaching
public class GtMoviesApplication {

	public static void main(String[] args) {
		SpringApplication.run(GtMoviesApplication.class, args);
	}
}
