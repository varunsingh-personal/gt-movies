package com.gt.movies.gtmovies.controllers;

import com.gt.movies.gtmovies.dtos.MoviePrefDto;
import com.gt.movies.gtmovies.exceptions.ObjectDoesNotExistException;
import com.gt.movies.gtmovies.exceptions.SearchValidationException;
import com.gt.movies.gtmovies.services.search.SearchService;
import com.gt.movies.gtmovies.services.user.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("movies")
public class SearchController {

    private SearchService searchService;

    private UserService userService;

    @Autowired
    public SearchController(SearchService searchService, UserService userService) {
        this.searchService = searchService;
        this.userService = userService;
    }

    private Logger logger = LoggerFactory.getLogger(SearchController.class);

    @GetMapping(value = "/user/{userId}/search")
    public List<String> searchMovies(@PathVariable String userId, @RequestParam(value = "text") String searchTerms) throws ObjectDoesNotExistException, SearchValidationException {
        this.logger.trace("Received search request for userId: {} and search: {}", userId, searchTerms);
        List<String> results = this.searchService.searchMovies(userId, searchTerms);
        this.logger.trace("Returning results for userId: {} and searchTerms: {}. Results: {}", userId, searchTerms, results);
        return results;
    }

    @GetMapping(value = "/users")
    public List<MoviePrefDto> getUserPreference() {
        this.logger.trace("Received search request for top movies of all users");
        List<MoviePrefDto> moviePrefDtos = this.userService.getTopMovies();
        this.logger.trace("results: {}", moviePrefDtos);
        return moviePrefDtos;
    }
}
