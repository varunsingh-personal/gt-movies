package com.gt.movies.gtmovies.dtos;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.gt.movies.gtmovies.models.User;

public class MoviePrefDto {

    @JsonProperty("user")
    private String userId;

    private List<String> movies;

    public MoviePrefDto() {
    }

    public MoviePrefDto(String userId, List<String> movies) {
        this.userId = userId;
        this.movies = movies;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public List<String> getMovies() {
        return movies;
    }

    public void setMovies(List<String> movies) {
        this.movies = movies;
    }

    public static MoviePrefDto of(User user) {
        MoviePrefDto moviePrefDto = new MoviePrefDto();
        if (moviePrefDto.getUserId() == null) {
            moviePrefDto.setUserId(user.getUserId());
        }

        moviePrefDto.setMovies(new ArrayList<>(user.getTopMovies()));
        Collections.sort(moviePrefDto.getMovies());
        return moviePrefDto;
    }

    @Override
    public String toString() {
        return "MoviePrefDto{" +
                "userId='" + userId + '\'' +
                ", movies=" + movies +
                '}';
    }
}
