package com.gt.movies.gtmovies.dtos;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * DTO for api errors
 */
public class ApiError {

    /**
     * error code to be thrown
     */
	@JsonProperty("error_code")
    private String errorCode;

    /**
     * message to be thrown
     */
    private String message;

    public static ApiError of(String errorCode, String message) {
        ApiError apiError = new ApiError();
        apiError.errorCode = errorCode;
        apiError.message = message;
        return apiError;
    }


    public String getErrorCode() {
        return errorCode;
    }

    public String getMessage() {
        return message;
    }
}
