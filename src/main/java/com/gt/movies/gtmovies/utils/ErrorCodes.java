package com.gt.movies.gtmovies.utils;

public class ErrorCodes {

    /**
     * Invalid language error code
     */
    public static final String ERROR_CODE_INVALID_LANG = "invalid_language";

    /**
     * User not found error code
     */
    public static final String ERROR_CODE_USER_NOT_FOUND = "user_not_found";
    public static final String ERROR_CODE_INVALID_SEARCH_TERMS = "invalid_search_terms";


    private ErrorCodes() {
    }
}
