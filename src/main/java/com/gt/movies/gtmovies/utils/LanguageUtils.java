package com.gt.movies.gtmovies.utils;

import com.gt.movies.gtmovies.exceptions.InvalidLanguageCodeException;

import java.util.Locale;

/**
 * Interface to define language related utility functions
 */
public interface LanguageUtils {

    /**
     * Returns Language name from ISO-639-1 language code. en: English, es: Spanish
     * @param code ISO-639-1 code
     * @return Language name
     * @throws InvalidLanguageCodeException
     */
    public static String languageFromCode(String code) throws InvalidLanguageCodeException {
        if (code == null || code.isEmpty()) {
            throw new InvalidLanguageCodeException("Language code null.", ErrorCodes.ERROR_CODE_INVALID_LANG);
        }

        //Adding special case for cn since it is in dataset but not in Locale. https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes
        if (code.equalsIgnoreCase("cn")) {
            code = "zh";
        }

        String retVal = new Locale(code).getDisplayLanguage();
        if (retVal.equalsIgnoreCase(code)) {
            throw new InvalidLanguageCodeException("Invalid language code: " + code + ". No Locale found.", ErrorCodes.ERROR_CODE_INVALID_LANG);
        }
        return retVal;
    }
}
