package com.gt.movies.gtmovies.config;

import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.mongodb.core.MongoTemplate;

import com.github.mongobee.Mongobee;

@Configuration
public class MongoDbConfig {

    private Logger logger = LoggerFactory.getLogger(MongoDbConfig.class);

    @Bean
    public Mongobee getMongobee(MongoTemplate mongoTemplate, Environment environment) {
        String dbUri = environment.getProperty("spring.data.mongodb.uri");
        String basePackage = environment.getProperty("mongobee.changelog.basepackage");
        this.logger.info(dbUri);
        Mongobee mongobee = new Mongobee(Objects.requireNonNull(dbUri));
        mongobee.setMongoTemplate(mongoTemplate);
        mongobee.setSpringEnvironment(environment);
        mongobee.setEnabled(true);
        mongobee.setChangeLogsScanPackage(Objects.requireNonNull(basePackage));
        this.logger.info("Configured mongobee with: {} basePackage: {}", dbUri, basePackage);
        return mongobee;
    }
}
