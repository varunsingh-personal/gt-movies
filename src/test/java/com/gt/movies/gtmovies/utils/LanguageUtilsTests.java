package com.gt.movies.gtmovies.utils;

import com.gt.movies.gtmovies.exceptions.InvalidLanguageCodeException;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class LanguageUtilsTests {

    @Test
    public void testValidLangCode() throws InvalidLanguageCodeException {
        String lang = LanguageUtils.languageFromCode("cn");
        Assert.assertEquals("Chinese", lang);
    }

    @Test(expected = InvalidLanguageCodeException.class)
    public void testInValidLangCode() throws InvalidLanguageCodeException {
        String lang = LanguageUtils.languageFromCode("xy");
    }

    @Test(expected = InvalidLanguageCodeException.class)
    public void testNullCode() throws InvalidLanguageCodeException {
        String lang = LanguageUtils.languageFromCode(null);
    }

    @Test(expected = InvalidLanguageCodeException.class)
    public void testBlankCode() throws InvalidLanguageCodeException {
        String lang = LanguageUtils.languageFromCode("");
    }
}
