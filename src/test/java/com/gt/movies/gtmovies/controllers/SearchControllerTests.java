package com.gt.movies.gtmovies.controllers;

import com.github.mongobee.Mongobee;
import com.gt.movies.gtmovies.exceptions.ObjectDoesNotExistException;
import com.gt.movies.gtmovies.exceptions.SearchValidationException;
import com.gt.movies.gtmovies.repositories.UserRepository;
import com.gt.movies.gtmovies.services.search.SearchService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.Collections;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(SearchController.class)
@ActiveProfiles("test")
public class SearchControllerTests {

    @MockBean
    private SearchService searchService;
    
    @MockBean
    private UserRepository userRepository;
    
    @MockBean
    private MongoTemplate mongoTemplate;
    
    @MockBean 
    private Mongobee mongobee;

    @Autowired
    private SearchController searchController;

    @Autowired
    private MockMvc mockMvc;


    @Before
    public void setup() throws SearchValidationException, ObjectDoesNotExistException {

        Mockito.when(searchService.searchMovies("1", "avatar")).thenThrow(ObjectDoesNotExistException.class);

        Mockito.when(searchService.searchMovies("2", "pirates")).thenThrow(SearchValidationException.class);

        Mockito.when(searchService.searchMovies("3", "avatar")).thenReturn(Arrays.asList("Avatar", "Pirates"));

        Mockito.when(searchService.searchMovies("4", "avatar")).thenReturn(Collections.emptyList());

    }

    @Test
    public void testInvalidUserException() throws Exception {
        this.mockMvc.perform(get("/movies/user/1/search?text=avatar")).andDo(print()).andExpect(status().isNotFound());
    }

    @Test
    public void testInvalidSearchException() throws Exception {
        this.mockMvc.perform(get("/movies/user/2/search?text=pirates")).andDo(print()).andExpect(status().isBadRequest());
    }

    @Test
    public void testValidSearchResult() throws Exception {
        this.mockMvc.perform(get("/movies/user/3/search?text=avatar")).andDo(print()).andExpect(status().isOk()).andExpect(content().json("[\"Avatar\",\"Pirates\"]"));
    }

    @Test
    public void testEmptySearchResult() throws Exception {
        this.mockMvc.perform(get("/movies/user/4/search?text=avatar")).andDo(print()).andExpect(status().isOk()).andExpect(content().json("[]"));
    }

    @Test
    public void testIncorrectUrl() throws Exception {
        this.mockMvc.perform(get("/movies/1/sear")).andDo(print()).andExpect(status().isNotFound());
    }

}
