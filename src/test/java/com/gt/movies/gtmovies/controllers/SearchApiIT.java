package com.gt.movies.gtmovies.controllers;

import com.gt.movies.gtmovies.db.migration.MovieImportMigration;
import com.gt.movies.gtmovies.db.migration.UserImportMigration;
import com.gt.movies.gtmovies.db.migration.UserPrefMigration;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.env.Environment;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, properties = "com.gt.movies.gtmovies.seedfilepath=integration-seed.csv")
@ActiveProfiles("test")
public class SearchApiIT {

    @LocalServerPort
    private int port;

    TestRestTemplate testRestTemplate = new TestRestTemplate();

    private HttpHeaders httpHeaders = new HttpHeaders();

    private Logger logger = LoggerFactory.getLogger(SearchApiIT.class);

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private Environment environment;

    @Before
    public void setup() {

        this.teardown();

        MovieImportMigration movieImportMigration = new MovieImportMigration();
        movieImportMigration.importMovies(this.mongoTemplate, this.environment);

        UserImportMigration userImportMigration = new UserImportMigration();
        userImportMigration.importUsers(this.mongoTemplate, this.environment);

        UserPrefMigration userPrefMigration = new UserPrefMigration();
        userPrefMigration.calculatePrefRank(this.mongoTemplate, this.environment);

    }

    @Test
    public void IT_testSearchApi() {
        String response = this.testRestTemplate.getForEntity("http://localhost:" + this.port + "/movies/user/101/search?text=johnny+depp", String.class).getBody();
        logger.info(response);
    }

    @After
    public void teardown() {
        this.mongoTemplate.dropCollection("dbchangelog");
        this.mongoTemplate.dropCollection("mongobeelock");
        this.mongoTemplate.dropCollection("movies");
        this.mongoTemplate.dropCollection("moviepreferences");
        this.mongoTemplate.dropCollection("users");
    }

}
