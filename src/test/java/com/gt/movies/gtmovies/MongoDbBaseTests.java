package com.gt.movies.gtmovies;


import com.gt.movies.gtmovies.db.migration.MovieImportMigration;
import com.gt.movies.gtmovies.db.migration.UserImportMigration;
import com.gt.movies.gtmovies.models.Movie;
import com.gt.movies.gtmovies.models.User;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.core.env.Environment;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataMongoTest
@ActiveProfiles("test")
public class MongoDbBaseTests {

    @Autowired
    protected MongoTemplate mongoTemplate;

    @Autowired
    protected Environment environment;

    @Before
    public void setup() {

    }

    /**
     * Return query for a user and movie
     * @param userId
     * @param movieId
     * @return
     */
    public static Query getMoviePrefQuery(String userId, String movieId) {
        Query movPrefQuery = new Query();
        movPrefQuery.addCriteria(Criteria.where(User.KEY_USER_USER_ID).is(userId).and(Movie.KEY_MOVIE_MOVIE_ID).is(movieId));
        return movPrefQuery;
    }

    /**
     * Deletes all collections and imports movies and users again
     */
    public void dropAllDatabases() {
        this.mongoTemplate.dropCollection("dbchangelog");
        this.mongoTemplate.dropCollection("mongobeelock");
        this.mongoTemplate.dropCollection("movies");
        this.mongoTemplate.dropCollection("moviepreferences");
        this.mongoTemplate.dropCollection("users");
    }

    /**
     * Imports movies into mongodb
     */
    public void importMovies() {
        MovieImportMigration movieImportMigration = new MovieImportMigration();
        movieImportMigration.importMovies(this.mongoTemplate, this.environment);
    }

    /**
     * Imports users into mongodb
     */
    public void importUsers() {
        UserImportMigration userImportMigration = new UserImportMigration();
        userImportMigration.importUsers(this.mongoTemplate, this.environment);
    }

    /**
     * Prepares data for testing
     */
    public void resetDatabase() {
        this.dropAllDatabases();
        this.importMovies();
        this.importUsers();
    }

    @Test
    public void emptyTest() {
        Assert.assertTrue(true);
    }

    @After
    public void teardown() {
        this.dropAllDatabases();
    }
}
