package com.gt.movies.gtmovies.db.migration;

import com.gt.movies.gtmovies.MongoDbBaseTests;
import com.gt.movies.gtmovies.models.MoviePreference;
import com.gt.movies.gtmovies.models.User;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.List;

public class MoviePrefCreationTests extends MongoDbBaseTests {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private Environment environment;

    private Logger logger = LoggerFactory.getLogger(this.getClass());


    @Before
    public void setup() {
        super.resetDatabase();
        UserPrefMigration userPrefMigration = new UserPrefMigration();
        userPrefMigration.calculatePrefRank(this.mongoTemplate, this.environment);
        userPrefMigration.addTopMoviesToUser(this.mongoTemplate, this.environment);
    }

    @Test
    public void testUserPrefMigrationCount() {
        List<MoviePreference> moviePreferenceList = this.mongoTemplate.findAll(MoviePreference.class);

        Assert.assertEquals(14, moviePreferenceList.size());

    }

    @Test
    public void testUserPrefMigrationMaxMatch() {

        MoviePreference user100Mov19995Pref = this.mongoTemplate.findOne(getMoviePrefQuery("100", "19995"), MoviePreference.class);

        Assert.assertEquals(5, user100Mov19995Pref.getRank());
    }

    @Test
    public void testUserPrefMigrationPartMatch() {

        MoviePreference user101Mov19995Pref = this.mongoTemplate.findOne(MongoDbBaseTests.getMoviePrefQuery("101", "19995"), MoviePreference.class);
        MoviePreference user102Mov19995Pref = this.mongoTemplate.findOne(MongoDbBaseTests.getMoviePrefQuery("102", "19995"), MoviePreference.class);
        MoviePreference user102Mov285Pref = this.mongoTemplate.findOne(MongoDbBaseTests.getMoviePrefQuery("102", "285"), MoviePreference.class);

        Assert.assertEquals(2, user101Mov19995Pref.getRank());
        Assert.assertEquals(0, user102Mov19995Pref.getRank());
        Assert.assertEquals(1, user102Mov285Pref.getRank());
    }

    @Test
    public void testCastCount() {
        MoviePreference user101Mov19995Pref = this.mongoTemplate.findOne(MongoDbBaseTests.getMoviePrefQuery("101", "19995"), MoviePreference.class);
        Assert.assertEquals(83, user101Mov19995Pref.getCast().size());
        this.logger.info(user101Mov19995Pref.getCast().toString());
    }

    @Test
    public void testMovieDirectorsCount() {
        MoviePreference user101Mov19995Pref = this.mongoTemplate.findOne(MongoDbBaseTests.getMoviePrefQuery("101", "19995"), MoviePreference.class);
        Assert.assertEquals(153, user101Mov19995Pref.getCrew().size());
        this.logger.info(user101Mov19995Pref.getCrew().toString());
    }

    @Test
    public void testNoTopMoviesCount() {
        User user = this.mongoTemplate.findOne(Query.query(Criteria.where(User.KEY_USER_USER_ID).is("103")), User.class);
        assert user != null;
        Assert.assertEquals(0, user.getTopMovies().size());
    }

    @Test
    public void test1TopMoviesCount() {
        User user = this.mongoTemplate.findOne(Query.query(Criteria.where(User.KEY_USER_USER_ID).is("100")), User.class);
        assert user != null;
        Assert.assertEquals(1, user.getTopMovies().size());
    }

    @Test
    public void testMultiTopMoviesCount() {
        User user = this.mongoTemplate.findOne(Query.query(Criteria.where(User.KEY_USER_USER_ID).is("106")), User.class);
        assert user != null;
        Assert.assertEquals(2, user.getTopMovies().size());
    }

    @After
    public void tearDown() {
        this.dropAllDatabases();
    }
}
