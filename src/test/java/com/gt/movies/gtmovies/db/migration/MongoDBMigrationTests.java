package com.gt.movies.gtmovies.db.migration;

import com.github.mongobee.Mongobee;
import com.gt.movies.gtmovies.MongoDbBaseTests;
import com.gt.movies.gtmovies.models.Movie;
import com.gt.movies.gtmovies.models.User;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.List;

public class MongoDBMigrationTests extends MongoDbBaseTests {


    @Test
    public void testMovieImportMigration() {
        MovieImportMigration movieImportMigration = new MovieImportMigration();
        movieImportMigration.importMovies(this.mongoTemplate, this.environment);
        List<Movie> movies = this.mongoTemplate.findAll(Movie.class);

        Assert.assertEquals(2, movies.size());
        Assert.assertEquals("Stephen E. Rivkin", movies.get(0).getCrew().get(0).getName());
        Assert.assertEquals("Editing", movies.get(0).getCrew().get(0).getDepartment());
        Assert.assertEquals("Editor", movies.get(0).getCrew().get(0).getJob());
    }

    @Test
    public void testUserImportMigration() {
        UserImportMigration userImportMigration = new UserImportMigration();
        userImportMigration.importUsers(this.mongoTemplate, this.environment);
        List<User> users = this.mongoTemplate.findAll(User.class);
        Query query = new Query();
        query.addCriteria(Criteria.where(User.KEY_USER_USER_ID).is("100"));
        User user = this.mongoTemplate.findOne(query, User.class);
        Assert.assertEquals(7, users.size());
        Assert.assertNotNull(user);
        Assert.assertEquals("Denzel Washington", user.getFavActors().get(0));
        Assert.assertEquals("Steven Spielberg", user.getFavDirectors().get(0));

    }

    @After
    public void teardown() {
        super.teardown();

    }

}
