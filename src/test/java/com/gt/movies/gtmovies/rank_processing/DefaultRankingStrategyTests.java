package com.gt.movies.gtmovies.rank_processing;

import com.gt.movies.gtmovies.factories.MovieFactory;
import com.gt.movies.gtmovies.factories.UserFactory;
import com.gt.movies.gtmovies.models.Movie;
import com.gt.movies.gtmovies.models.User;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.UUID;

@RunWith(SpringRunner.class)
public class DefaultRankingStrategyTests {


    @Test
    public void testNoPreferenceExists() {
        User user = UserFactory.userWithNoPreference();
        Movie.MovieBuilder movieBuilder = new Movie.MovieBuilder("1");
        Movie movie = MovieFactory.movieWith(UUID.randomUUID().toString(),
                "Avengers",
                "en",
                "[{\"id\": 1, \"name\": \"Johny Depp\"}]",
                "[{\"id\": 1, \"department\": \"directing\", \"job\": \"Director\", \"name\": \"Quentin Tarantino\"}]");

        RankingStrategy rankingStrategy = new DefaultRankingStrategy(movie, user);
        int rank = rankingStrategy.rank();
        Assert.assertEquals(0, rank);
    }

    @Test
    public void testNoMovieDetailsExists() {
        User user = UserFactory.userWithAllPreferences();
        Movie movie = MovieFactory.movieWithNoDetails();

        RankingStrategy rankingStrategy = new DefaultRankingStrategy(movie, user);
        int rank = rankingStrategy.rank();
        Assert.assertEquals(0, rank);
    }

    @Test
    public void testMatchingLanguagePrefExists() {
        User user = UserFactory.userWithLanguagePref(Arrays.asList("English"));
        Movie movie = MovieFactory.movieWithLanguage("en");

        RankingStrategy rankingStrategy = new DefaultRankingStrategy(movie, user);
        int rank = rankingStrategy.rank();
        Assert.assertEquals(0, rank);
    }

    @Test
    public void testNonMatchingLanguagePrefExists() {
        User user = UserFactory.userWithLanguagePref(Arrays.asList("English"));
        Movie movie = MovieFactory.movieWithLanguage("es");

        RankingStrategy rankingStrategy = new DefaultRankingStrategy(movie, user);
        int rank = rankingStrategy.rank();
        Assert.assertEquals(0, rank);
    }

    @Test
    public void testMatchingActorPrefExists() {

        User user = UserFactory.userWithCastPref(Arrays.asList("Johny Depp"));
        Movie movie = MovieFactory.movieWithCast("[{\"id\": 1, \"name\": \"Johny Depp\"}]");

        RankingStrategy rankingStrategy = new DefaultRankingStrategy(movie, user);
        int rank = rankingStrategy.rank();
        Assert.assertEquals(1, rank);
    }

    @Test
    public void testNonMatchingActorPrefExists() {

        User user = UserFactory.userWithCastPref(Arrays.asList("Marlon Brando"));
        Movie movie = MovieFactory.movieWithCast("[{\"id\": 1, \"name\": \"Johny Depp\"}]");

        RankingStrategy rankingStrategy = new DefaultRankingStrategy(movie, user);
        int rank = rankingStrategy.rank();
        Assert.assertEquals(0, rank);
    }

    @Test
    public void testMatchingDirectorPrefExists() {

        User user = UserFactory.userWithCrewPref(Arrays.asList("Quentin Tarantino"));
        Movie movie = MovieFactory.movieWithCrew("[{\"id\": 1, \"department\": \"directing\", \"job\": \"Director\", \"name\": \"Quentin Tarantino\"}]");

        RankingStrategy rankingStrategy = new DefaultRankingStrategy(movie, user);
        int rank = rankingStrategy.rank();
        Assert.assertEquals(1, rank);
    }

    @Test
    public void testNonMatchingDirectorPrefExists() {
        User user = UserFactory.userWithCrewPref(Arrays.asList("Guy Ritchie}"));
        Movie movie = MovieFactory.movieWithCrew("[{\"id\": 1, \"department\": \"directing\", \"job\": \"Director\", \"name\": \"Quentin Tarantino\"}]");

        RankingStrategy rankingStrategy = new DefaultRankingStrategy(movie, user);
        int rank = rankingStrategy.rank();
        Assert.assertEquals(0, rank);
    }

    @Test
    public void testNonMatchingCrewJobExists() {

        User user = UserFactory.userWithCrewPref(Arrays.asList("Guy Ritchie}"));

        Movie movie = MovieFactory.movieWithCrew("[{\"id\": 1, \"department\": \"editing\", \"job\": \"Editor\", \"name\": \"Guy Ritchie\"}]");

        RankingStrategy rankingStrategy = new DefaultRankingStrategy(movie, user);
        int rank = rankingStrategy.rank();
        Assert.assertEquals(0, rank);
    }

    @Test
    public void testAllMatchesExists() {
        User user = UserFactory.userWithPreference(UUID.randomUUID().toString(),
                Arrays.asList("Spanish"),
                Arrays.asList("Johny Depp", "Marlon Brando"),
                Arrays.asList("Quentin Tarantino", "Ridley Scott"));

        Movie movie = MovieFactory.movieWith(UUID.randomUUID().toString(),
                "Avengers",
                "es",
                "[{\"id\": 1, \"name\": \"Johny Depp\"}, {\"id\": 2, \"name\": \"Marlon Brando\"}]",
                "[{\"id\": 1, \"department\": \"directing\", \"job\": \"Director\", \"name\": \"Quentin Tarantino\"}, " +
                        "{\"id\": 2, \"department\": \"directing\", \"job\": \"Director\", \"name\": \"Ridley Scott\"}]");

        RankingStrategy rankingStrategy = new DefaultRankingStrategy(movie, user);
        int rank = rankingStrategy.rank();
        Assert.assertEquals(4, rank);
    }
}
