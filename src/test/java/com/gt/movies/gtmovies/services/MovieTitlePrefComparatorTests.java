package com.gt.movies.gtmovies.services;

import com.gt.movies.gtmovies.models.MoviePreference;
import com.gt.movies.gtmovies.services.search.MoviePrefComparator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class MovieTitlePrefComparatorTests {

    @Autowired
    private MoviePrefComparator moviePrefComparator;

    @Before
    public void setup() {

    }

    @Test
    public void testMovieComparison() {
        MoviePreference.MoviePreferenceBuilder aBuilder = new MoviePreference.MoviePreferenceBuilder("1", "1");
        MoviePreference aMoviePref = aBuilder.withRank(1).withMovieTitle("A").withCast(new ArrayList<>()).withCrew(new ArrayList<>()).build();

        MoviePreference.MoviePreferenceBuilder bBuilder = new MoviePreference.MoviePreferenceBuilder("1", "1");
        MoviePreference bMoviePref = bBuilder.withRank(1).withMovieTitle("B").withCast(new ArrayList<>()).withCrew(new ArrayList<>()).build();

        Assert.assertEquals(1, moviePrefComparator.compare(bMoviePref, aMoviePref));

    }
}
