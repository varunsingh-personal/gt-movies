package com.gt.movies.gtmovies.services;

import com.gt.movies.gtmovies.exceptions.SearchValidationException;
import com.gt.movies.gtmovies.services.search.SearchTermProcessingService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class SearchTermProcessingServiceTests {

    @Autowired
    private SearchTermProcessingService searchTermProcessingService;

    private Logger logger = LoggerFactory.getLogger(SearchTermProcessingServiceTests.class);


    @Test(expected = SearchValidationException.class)
    public void testProcessingNull() throws SearchValidationException {
        this.searchTermProcessingService.processSearchTerms(null);

    }

    @Test(expected = SearchValidationException.class)
    public void testProcessingEmpty() throws SearchValidationException {
        this.searchTermProcessingService.processSearchTerms("");

    }

    @Test
    public void testProcessSingle() throws SearchValidationException {
        List<String> searchTerms = this.searchTermProcessingService.processSearchTerms("ABC");
        Assert.assertEquals(1, searchTerms.size());
        Assert.assertEquals("abc", searchTerms.get(0));
    }

    @Test
    public void testProcessMulti() throws SearchValidationException {
        List<String> searchTerms = this.searchTermProcessingService.processSearchTerms("ABC, PQR");
        this.logger.info(searchTerms.toString());
        Assert.assertEquals(2, searchTerms.size());
        Assert.assertEquals("abc", searchTerms.get(0));
        Assert.assertEquals("pqr", searchTerms.get(1));
    }
}
