package com.gt.movies.gtmovies.services;


import com.gt.movies.gtmovies.exceptions.ObjectDoesNotExistException;
import com.gt.movies.gtmovies.exceptions.SearchValidationException;
import com.gt.movies.gtmovies.models.Cast;
import com.gt.movies.gtmovies.models.Crew;
import com.gt.movies.gtmovies.models.MoviePreference;
import com.gt.movies.gtmovies.models.User;
import com.gt.movies.gtmovies.repositories.MoviePreferenceRepository;
import com.gt.movies.gtmovies.repositories.UserRepository;
import com.gt.movies.gtmovies.services.search.SearchService;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class SearchServicesTests {
    @MockBean
    private MoviePreferenceRepository moviePreferenceRepository;

    @MockBean
    private UserRepository userRepository;

    @Autowired
    private SearchService searchService;


    @Before
    public void setup() {
        MoviePreference.MoviePreferenceBuilder builder = new MoviePreference.MoviePreferenceBuilder("1", "1");
        MoviePreference pref1 = builder.withCast(Arrays.asList(new Cast("1", "johny depp"))).withCrew(Arrays.asList(new Crew("2", "steven spielberg", "directing", "director"))).withMovieTitle("Avatar").withRank(1).build();
        MoviePreference pref2 = builder.withCast(Arrays.asList(new Cast("1", "johny depp"))).withCrew(Arrays.asList(new Crew("3", "guy ritchie", "directing", "director"))).withMovieTitle("Pirates").withRank(3).build();
        MoviePreference pref3 = builder.withCast(Arrays.asList(new Cast("1", "tom hanks"))).withCrew(Arrays.asList(new Crew("3", "steven spielberg", "directing", "director"))).withMovieTitle("Toy Story").withRank(4).build();
//        Mockito.when(this.moviePreferenceRepository.defaultSearch("1", Arrays.asList("depp", "steven"), 1, Integer.MAX_VALUE)).thenReturn(Stream.of(pref2, pref1, pref3));

        MoviePreference nopref1 = builder.withCast(Arrays.asList(new Cast("1", "johny depp"))).withCrew(Arrays.asList(new Crew("2", "steven spielberg", "directing", "director"))).withMovieTitle("Avenger").withRank(0).build();
        MoviePreference nopref2 = builder.withCast(Arrays.asList(new Cast("1", "johny depp"))).withCrew(Arrays.asList(new Crew("3", "guy ritchie", "directing", "director"))).withMovieTitle("Mission Impossible").withRank(0).build();
        MoviePreference nopref3 = builder.withCast(Arrays.asList(new Cast("1", "tom hanks"))).withCrew(Arrays.asList(new Crew("3", "steven spielberg", "directing", "director"))).withMovieTitle("Jumanji").withRank(0).build();
//        Mockito.when(this.moviePreferenceRepository.defaultSearch("1", Arrays.asList("depp", "steven"), 0, 0)).thenReturn(Stream.of(nopref2, nopref1, nopref3));

        Mockito.when(this.moviePreferenceRepository.defaultSearch("1", Arrays.asList("depp", "steven"), 0, Integer.MAX_VALUE)).thenReturn(Stream.of(pref2, pref1, pref3, nopref2, nopref1, nopref3));

        MoviePreference onlynopref1 = builder.withCast(Arrays.asList(new Cast("1", "tom cruise"))).withCrew(Arrays.asList(new Crew("2", "steven spielberg", "directing", "director"))).withMovieTitle("Godfather").withRank(0).build();
        MoviePreference onlynopref2 = builder.withCast(Arrays.asList(new Cast("1", "tom cruise"))).withCrew(Arrays.asList(new Crew("3", "guy ritchie", "directing", "director"))).withMovieTitle("Top gun").withRank(0).build();
        MoviePreference onlynopref3 = builder.withCast(Arrays.asList(new Cast("1", "tom cruise"))).withCrew(Arrays.asList(new Crew("3", "steven spielberg", "directing", "director"))).withMovieTitle("Sicario").withRank(0).build();
        Mockito.when(this.moviePreferenceRepository.defaultSearch("1", Arrays.asList("cruise"), 0, Integer.MAX_VALUE)).thenReturn(Stream.of(onlynopref1, onlynopref2, onlynopref3));

        MoviePreference onlypref1 = builder.withCast(Arrays.asList(new Cast("1", "al pacino"))).withCrew(Arrays.asList(new Crew("2", "steven spielberg", "directing", "director"))).withMovieTitle("John Wick").withRank(0).build();
        MoviePreference onlypref2 = builder.withCast(Arrays.asList(new Cast("1", "al pacino"))).withCrew(Arrays.asList(new Crew("3", "guy ritchie", "directing", "director"))).withMovieTitle("Narnia").withRank(0).build();
        MoviePreference onlypref3 = builder.withCast(Arrays.asList(new Cast("1", "al pacino"))).withCrew(Arrays.asList(new Crew("3", "steven spielberg", "directing", "director"))).withMovieTitle("Fight Club").withRank(0).build();
        Mockito.when(this.moviePreferenceRepository.defaultSearch("1", Arrays.asList("pacino"), 0, Integer.MAX_VALUE)).thenReturn(Stream.of(onlypref1, onlypref2, onlypref3));


        User.UserBuilder userBuilder = new User.UserBuilder("1");
        User user = userBuilder.withPreferredLanguages(new ArrayList<>())
                .withfavoriteActors(new ArrayList<>())
                .withfavoriteDirectors(new ArrayList<>()).build();
        Mockito.when(userRepository.findByUserId("1")).thenReturn(user);



    }

    @Test
    public void testResultSequenceWithPrefNoPref() throws SearchValidationException, ObjectDoesNotExistException {
        List<String> results = searchService.searchMovies("1", "Depp, Steven");
        Assert.assertEquals(6, results.size());
        Assert.assertEquals("Avatar", results.get(0));
        Assert.assertEquals("Pirates", results.get(1));
        Assert.assertEquals("Toy Story", results.get(2));
        Assert.assertEquals("Avenger", results.get(3));
        Assert.assertEquals("Jumanji", results.get(4));
        Assert.assertEquals("Mission Impossible", results.get(5));
    }

    @Test
    public void testResultSequenceOnlyPref() throws SearchValidationException, ObjectDoesNotExistException {
        List<String> results = searchService.searchMovies("1", "Pacino");
        Assert.assertEquals(3, results.size());
        Assert.assertEquals("Fight Club", results.get(0));
        Assert.assertEquals("John Wick", results.get(1));
        Assert.assertEquals("Narnia", results.get(2));

    }

    @Test
    public void testResultSequenceOnlyNoPref() throws SearchValidationException, ObjectDoesNotExistException {
        List<String> results = searchService.searchMovies("1", "Cruise");
        Assert.assertEquals(3, results.size());
        Assert.assertEquals("Godfather", results.get(0));
        Assert.assertEquals("Sicario", results.get(1));
        Assert.assertEquals("Top gun", results.get(2));

    }

    @Test
    public void testNoResults() throws SearchValidationException, ObjectDoesNotExistException {
        List<String> results = searchService.searchMovies("1", "Test");
        Assert.assertEquals(0, results.size());
    }

    @After
    public void teardown() {

    }


}
