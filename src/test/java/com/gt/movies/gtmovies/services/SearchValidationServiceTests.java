package com.gt.movies.gtmovies.services;

import com.gt.movies.gtmovies.exceptions.ObjectDoesNotExistException;
import com.gt.movies.gtmovies.exceptions.SearchValidationException;
import com.gt.movies.gtmovies.models.User;
import com.gt.movies.gtmovies.repositories.UserRepository;
import com.gt.movies.gtmovies.services.search.SearchValidationService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class SearchValidationServiceTests {

    @MockBean
    private UserRepository userRepository;

    @Autowired
    private SearchValidationService searchValidationService;

    @Before
    public void setup() {
        User.UserBuilder builder = new User.UserBuilder("1");
        User user = builder.withPreferredLanguages(new ArrayList<>())
                .withfavoriteActors(new ArrayList<>())
                .withfavoriteDirectors(new ArrayList<>()).build();
        Mockito.when(userRepository.findByUserId("1")).thenReturn(user);

        Mockito.when(userRepository.findByUserId("2")).thenReturn(null);
    }

    @Test(expected = ObjectDoesNotExistException.class)
    public void testInvalidUserId() throws SearchValidationException, ObjectDoesNotExistException {
        this.searchValidationService.validateSearchRequest("2", Arrays.asList("a", "b"));
    }

    @Test(expected = ObjectDoesNotExistException.class)
    public void testNullUserId() throws SearchValidationException, ObjectDoesNotExistException {
        this.searchValidationService.validateSearchRequest(null, Arrays.asList("a", "b"));
    }

    @Test(expected = SearchValidationException.class)
    public void testNullSearchTerms() throws SearchValidationException, ObjectDoesNotExistException {
        this.searchValidationService.validateSearchRequest("1", null);
    }

    @Test(expected = SearchValidationException.class)
    public void testNullInSearchTerms() throws SearchValidationException, ObjectDoesNotExistException {
        this.searchValidationService.validateSearchRequest("1", Arrays.asList("a", null));
    }

    @Test
    public void testUnicodeInSearchTerms() throws SearchValidationException, ObjectDoesNotExistException {
        this.searchValidationService.validateSearchRequest("1", Collections.singletonList("Čeština"));
    }

    public void testValidSearchRequest() throws SearchValidationException, ObjectDoesNotExistException {
        this.searchValidationService.validateSearchRequest("1", Arrays.asList("a", "1"));
    }

    @After
    public void teardown() {

    }
}
