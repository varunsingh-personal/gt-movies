package com.gt.movies.gtmovies.repositories;

import com.gt.movies.gtmovies.MongoDbBaseTests;
import com.gt.movies.gtmovies.db.migration.UserPrefMigration;
import com.gt.movies.gtmovies.models.MoviePreference;
import org.apache.logging.log4j.util.PropertySource;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class MoviePreferenceRepositoryTests extends MongoDbBaseTests {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private Environment environment;

    @Autowired
    private MoviePreferenceRepository moviePreferenceRepository;

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Before
    public void setup() {
        this.resetDatabase();

        UserPrefMigration userPrefMigration = new UserPrefMigration();
        userPrefMigration.calculatePrefRank(this.mongoTemplate, this.environment);
        userPrefMigration.createMoviePrefIndices(this.mongoTemplate, this.environment);
    }

    @Test
    public void testMongoPrefRepo() {
        List<MoviePreference> results = this.moviePreferenceRepository.findAll();
        logger.info(results.toString());
        Assert.assertEquals(14, results.size());
    }

    @Test
    public void testNoMatchExists() {
        List<String> terms = Arrays.asList("test", "xyz", "abc");
        int minRank = 0;
        int maxRank = Integer.MAX_VALUE;
        String userId = "100";
        List<MoviePreference> results = this.moviePreferenceRepository.defaultSearch(userId, terms, minRank, maxRank).collect(Collectors.toList());
        Assert.assertEquals(0, results.size());
    }
//
    @Test
    public void testTitleMatchExists() {
        List<String> terms = Arrays.asList("avatar", "xyz", "abc");
        int minRank = 0;
        int maxRank = Integer.MAX_VALUE;
        String userId = "100";
        List<MoviePreference> results = this.moviePreferenceRepository.defaultSearch(userId, terms, minRank, maxRank).collect(Collectors.toList());
        this.logger.info(results.toString());
        Assert.assertEquals(1, results.size());
        Assert.assertEquals("Avatar", results.get(0).getMovieTitle());
    }

    @Test
    public void testCastMatchExists() {
        List<String> terms = Arrays.asList("Hanks", "Xyz", "Abc");
        int minRank = 0;
        int maxRank = Integer.MAX_VALUE;
        String userId = "106";
        List<MoviePreference> results = this.moviePreferenceRepository.defaultSearch(userId, terms, minRank, maxRank).collect(Collectors.toList());
        Assert.assertEquals(1, results.size());
        Assert.assertEquals("Avatar", results.get(0).getMovieTitle());
    }

    @Test
    public void testMultipleCastMatchExists() {
        List<String> terms = Arrays.asList("tom hanks", "johnny depp", "abc");
        int minRank = 0;
        int maxRank = Integer.MAX_VALUE;
        String userId = "106";
        List<MoviePreference> results = this.moviePreferenceRepository.defaultSearch(userId, terms, minRank, maxRank).collect(Collectors.toList());
        results.sort((prefLeft, prefRight) -> prefLeft.getMovieTitle().compareToIgnoreCase(prefRight.getMovieTitle()));
        Assert.assertEquals(2, results.size());
        Assert.assertEquals("Avatar", results.get(0).getMovieTitle());
        Assert.assertEquals("Pirates of the Caribbean: At World's End", results.get(1).getMovieTitle());
    }

    @Test
    public void testMultipleTermsSameMovieMatchExists() {
        List<String> terms = Arrays.asList("tom hanks", "avatar", "abc");
        int minRank = 0;
        int maxRank = Integer.MAX_VALUE;
        String userId = "106";
        List<MoviePreference> results = this.moviePreferenceRepository.defaultSearch(userId, terms, minRank, maxRank).collect(Collectors.toList());
        Assert.assertEquals(1, results.size());
        Assert.assertEquals("Avatar", results.get(0).getMovieTitle());
    }

    @Test
    public void testMultipleTermsMultipleMovieMatchExists() {
        List<String> terms = Arrays.asList("johnny depp", "tom hanks", "avatar");
        int minRank = 0;
        int maxRank = Integer.MAX_VALUE;
        String userId = "106";
        List<MoviePreference> results = this.moviePreferenceRepository.defaultSearch(userId, terms, minRank, maxRank).collect(Collectors.toList());
        results.sort((prefLeft, prefRight) -> prefLeft.getMovieTitle().compareToIgnoreCase(prefRight.getMovieTitle()));
        Assert.assertEquals(2, results.size());
        Assert.assertEquals("Avatar", results.get(0).getMovieTitle());
        Assert.assertEquals("Pirates of the Caribbean: At World\'s End", results.get(1).getMovieTitle());
    }

    @Test
    public void testCrewMatchExists() {
        List<String> terms = Arrays.asList("Steven Spielberg", "Quentin Tarantino", "abc");
        int minRank = 0;
        int maxRank = Integer.MAX_VALUE;
        String userId = "106";
        List<MoviePreference> results = this.moviePreferenceRepository.defaultSearch(userId, terms, minRank, maxRank).collect(Collectors.toList());
        Assert.assertEquals(2, results.size());
        results.sort((prefLeft, prefRight) -> prefLeft.getMovieTitle().compareToIgnoreCase(prefRight.getMovieTitle()));
        Assert.assertEquals("Avatar", results.get(0).getMovieTitle());
        Assert.assertEquals("Pirates of the Caribbean: At World\'s End", results.get(1).getMovieTitle());
    }

    @Test
    public void testCrewNonDirectorCrewNoMatch() {
        List<String> terms = Arrays.asList("stephen e. rivkin", "pqr", "abc");
        int minRank = 0;
        int maxRank = Integer.MAX_VALUE;
        String userId = "106";
        List<MoviePreference> results = this.moviePreferenceRepository.defaultSearch(userId, terms, minRank, maxRank).collect(Collectors.toList());
        Assert.assertEquals(0, results.size());
    }

    @Test
    public void testMovieWithPreferenceMatch() {
        List<String> terms = Arrays.asList("english", "denzel washington", "steven spielberg");
        int minRank = 1;
        int maxRank = Integer.MAX_VALUE;
        String userId = "100";
        List<MoviePreference> results = this.moviePreferenceRepository.defaultSearch(userId, terms, minRank, maxRank).collect(Collectors.toList());
        Assert.assertEquals(1, results.size());
        Assert.assertEquals("Avatar", results.get(0).getMovieTitle());
    }

    @Test
    public void testSearchWithNoPreferenceMatch() {
        List<String> terms = Arrays.asList("johnny depp");
        int minRank = 1;
        int maxRank = Integer.MAX_VALUE;
        String userId = "105";
        List<MoviePreference> results = this.moviePreferenceRepository.defaultSearch(userId, terms, minRank, maxRank).collect(Collectors.toList());
        Assert.assertEquals(0, results.size());
    }


    @After
    public void teardown() {
        this.dropAllDatabases();
    }
}
