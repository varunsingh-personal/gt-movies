package com.gt.movies.gtmovies.repositories;

import com.gt.movies.gtmovies.MongoDbBaseTests;
import com.gt.movies.gtmovies.models.User;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;

public class UserRepositoryTests extends MongoDbBaseTests {

    @Autowired
    private UserRepository userRepository;

    @Before
    public void setup() {
        User.UserBuilder builder = new User.UserBuilder("1");
        User user = builder.withfavoriteDirectors(Arrays.asList("abc"))
                .withfavoriteActors(Arrays.asList("pqr"))
                .withPreferredLanguages(Arrays.asList("en")).build();
        this.mongoTemplate.save(user);
    }

    @Test
    public void testFindValidUser() {
        User user = this.userRepository.findByUserId("1");
        Assert.assertNotNull(user);
    }

    @Test
    public void testFindInvalidUser() {
        User user = this.userRepository.findByUserId("2");
        Assert.assertNull(user);
    }

}
