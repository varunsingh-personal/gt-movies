package com.gt.movies.gtmovies.factories;

import com.gt.movies.gtmovies.models.User;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

/**
 * User factories to return user objects in various configs
 */
public class UserFactory {


    /**
     * Return user object with preferred languages, cast and directors
     * @param userId
     * @param languages preferred languages
     * @param cast preferred cast
     * @param directors preferred directors
     * @return user object with preferred languages, cast and directors
     */
    public static User userWithPreference(String userId, List<String> languages, List<String> cast, List<String> directors) {
        User.UserBuilder builder = new User.UserBuilder(userId);
        return builder.withPreferredLanguages(languages)
                .withfavoriteActors(cast)
                .withfavoriteDirectors(directors)
                .build();
    }
    /**
     *
     * @return user obj with no preferences
     */
    public static User userWithNoPreference() {
        return UserFactory.userWithPreference(UUID.randomUUID().toString(), null, null, null);
    }

    /**
     *
     * @return user obj with all default preferences
     */
    public static User userWithAllPreferences() {
        return UserFactory.userWithPreference(UUID.randomUUID().toString(),
                Arrays.asList("English", "Spanish"),
                Arrays.asList("Johny Depp", "Marlon Brando"),
                Arrays.asList("Quentin Tarantino", "Guy Ritchie"));
    }

    /**
     *
     * @param languageList list of preferred languages
     * @return Usre obj with preferred language
     */
    public static User userWithLanguagePref(List<String> languageList) {
        return UserFactory.userWithPreference(UUID.randomUUID().toString(), languageList, null, null);
    }

    /**
     *
     * @param cast list of preferred actors
     * @return user obj with preferred actors
     */
    public static User userWithCastPref(List<String> cast) {
        return UserFactory.userWithPreference(UUID.randomUUID().toString(), null, cast, null);
    }

    /**
     *
     * @param directors list of preferred directors
     * @return user obj with preferred directors
     */
    public static User userWithCrewPref(List<String> directors) {
        return UserFactory.userWithPreference(UUID.randomUUID().toString(), null, null, directors);
    }
}
