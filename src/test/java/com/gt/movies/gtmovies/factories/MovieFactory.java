package com.gt.movies.gtmovies.factories;

import com.gt.movies.gtmovies.models.Movie;

import java.util.UUID;

public class MovieFactory {

    /**
     * Creates movie obj with given params
     * @param id Id of movie
     * @param title title of movie
     * @param language language of movie
     * @param cast cast of movie
     * @param crew crew of movie
     * @return movie obj with passed params
     */
    public static Movie movieWith(String id, String title, String language, String cast, String crew) {
        Movie.MovieBuilder movieBuilder = new Movie.MovieBuilder(id);
        return movieBuilder.withTitle(title)
                .withLanguage(language)
                .withCast(cast)
                .withCrew(crew).build();
    }

    /**
     *
     * @return movie with no details, only id
     */
    public static Movie movieWithNoDetails() {
        return MovieFactory.movieWith(UUID.randomUUID().toString(), null, null, null, null);
    }

    /**
     *
     * @return movie with only langauge param
     */
    public static Movie movieWithLanguage(String language) {
        return MovieFactory.movieWith(UUID.randomUUID().toString(), null, language, null, null);
    }

    /**
     *
     * @return movie with only cast param
     */
    public static Movie movieWithCast(String cast) {
        return MovieFactory.movieWith(UUID.randomUUID().toString(), null, null, cast, null);
    }

    /**
     *
     * @return movie with only crew param
     */
    public static Movie movieWithCrew(String crew) {
        return MovieFactory.movieWith(UUID.randomUUID().toString(), null, null, null, crew);
    }


}
